package com.hunterarley.eventdistribution.testTwo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import com.hunterarley.eventdistribution.MainActivity
import com.hunterarley.eventdistribution.R

class TestTwoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_two)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestTwoActivity: dispatchTouchEvent")
        return super.dispatchTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestTwoActivity: onTouchEvent")
        return super.onTouchEvent(event)
    }
}
