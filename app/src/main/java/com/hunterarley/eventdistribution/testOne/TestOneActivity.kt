package com.hunterarley.eventdistribution.testOne

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import com.hunterarley.eventdistribution.MainActivity
import com.hunterarley.eventdistribution.R

class TestOneActivity : AppCompatActivity() {

    /**
     * 事件默认传递流程：
     * TestOneActivity: dispatchTouchEvent -> MyViewGroup: dispatchTouchEvent -> MyViewGroup: onInterceptTouchEvent ->
     * MyView: dispatchTouchEvent -> MyView: onTouchEvent -> MyViewGroup: onTouchEvent -> TestOneActivity: onTouchEvent
     *
     * 同一事件序列，如果子View（ViewGroup）没有处理该事件（没有消费事件），那么后续事件就不会再传递到子View中。
     * MainActivity: dispatchTouchEvent ->  MainActivity: onTouchEvent
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_one)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestOneActivity: dispatchTouchEvent")
        return super.dispatchTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestOneActivity: onTouchEvent")
        return super.onTouchEvent(event)
    }
}
