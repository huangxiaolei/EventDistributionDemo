package com.hunterarley.eventdistribution.testThree

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import com.hunterarley.eventdistribution.MainActivity
import com.hunterarley.eventdistribution.R

class TestThreeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_three)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestThreeActivity: dispatchTouchEvent")
        return super.dispatchTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        Log.d(MainActivity.LOG,"TestThreeActivity: onTouchEvent")
        return super.onTouchEvent(event)
    }
}
