package com.hunterarley.eventdistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import com.hunterarley.eventdistribution.testOne.TestOneActivity
import com.hunterarley.eventdistribution.testThree.TestThreeActivity
import com.hunterarley.eventdistribution.testTwo.TestTwoActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * 1. 验证事件传递流程
 * 2. 验证cancel事件
 * 3. 验证ViewGroup事件拦截
 */
class MainActivity : AppCompatActivity() {
    companion object{
        const val LOG: String = "myTouchDemo"
    }


    /**
     * ViewGroup:dispatchTouchEvent
     *  1.判断是否需要拦截事件
     *  2.在当前ViewGroup中找到用户真正点击的View
     *  3.分发事件到View上
     */


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnOne.setOnClickListener { startActivity(Intent(this,TestOneActivity::class.java)) }
        btnTwo.setOnClickListener { startActivity(Intent(this, TestTwoActivity::class.java)) }
        btnThree.setOnClickListener { startActivity(Intent(this,TestThreeActivity::class.java)) }
    }


}
