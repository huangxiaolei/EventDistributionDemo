# Android事件分发机制

## 一、什么是事件分发？

- 通过手指对屏幕的触摸来完成整个交互过程；
- 用户通过屏幕与手机交互的时候，每一次点击、长按、移动等都是一个事件；
- 事件分发机制：某一个事件从屏幕传递各个View，由View来使用这一事件（消费事件）或者忽略这一事件（不消费事件），这整个过程的控制。

#### 1.1、事件分发的对象是？

- 系统把事件封装成MotionEvent对象，事件分发的过程就是MotionEvent分发的过程。

#### 1.2、事件的类型

- 按下（ACTION_DOWN）
- 移动（ACTION_MOVE）
- 抬起（ACTION_UP）
- 取消（ACTION_CANCEL）

#### 1.3、事件序列

- 从手指按下屏幕开始，到手指离开屏幕所产生的一系列事件。

#### 1.4、传递层级

- Activity -> Window -> DecorView -> ViewGroup -> View

#### 1.5、主要传递对象及顺序

- Activity
- ViewGroup
- View

## 二、Activity中的主要方法

- dispatchTouchEvent(MotionEvent ev)
- onTouchEvent(MotionEvent ev)

## 三、ViewGroup中的主要方法

- dispatchTouchEvent(MotionEvent ev)
- onInterceptTouchEvent(MotionEvent ev)
- onTouchEvent(MotionEvent ev)：ViewGroup的父类View中实现


## 四、View中的主要方法

- dispatchTouchEvent(MotionEvent ev)
- onTouchEvent(MotionEvent ev)

## 五、总结

- Activity的事件分发流程
- ViewGroup的事件分发流程
- View的事件分发流程